export { default as NavBar } from "./sections/NavBar";
export { default as Header } from "./sections/Header";
export { default as Footer } from "./sections/Footer";
export { default as SideBar } from "./sections/SideBar";
export { default as Products } from "./sections/Products";
export { default as Products2 } from "./sections/Products2";
